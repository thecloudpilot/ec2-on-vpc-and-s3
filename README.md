# ec2-on-vpc-and-s3

This repository contains code to create a AWS VPC, an EC2 instance deployed on its subnet and an S3 bucket through GitLab pipeline using Terraform.

Pre-requisites:
 - Have your own AWS account(free-tier also possible).
 - Create a user with necessary permission for creating an EC2 instance, if not present.

Steps:
1. Clone the repository to your GitLab project.
2. Update main.tf file.
3. Change the ACCESS KEY and SECRET KEY with your keys in the code.
4. Remember to give a globally unique name to the bucket(else, it will cause a 400 error and bucket won't be created).
5. Commit the changes.
6. Run the CI/CD pipeline.
