terraform {
    required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~>3.0"
      }
    }
}

provider "aws" {
    region = "us-east-2"
    access_key = "#ENTER ACCESS KEY"
    secret_key = "#ENTER SECRET KEY"
}

resource "aws_vpc" "testnet" {
    cidr_block = "172.16.0.0/18"
}

resource "aws_subnet" "testsubnet" {
    vpc_id = aws_vpc.testnet.id
    cidr_block = "172.16.10.0/24"
    availability_zone = "us-east-2a"
}

resource "aws_network_interface" "testinterface" {
  subnet_id   = aws_subnet.testsubnet.id
  private_ips = ["172.16.10.100"]
}

# IMPORTANT - Bucket name should be globally unique
resource "aws_s3_bucket" "ec2-on-vpc-and-s3-bucket"  {
  bucket = "ec2-on-vpc-and-s3-test-bucket"
  acl    = "public-read"
}

resource "aws_instance" "testvm" {
    ami = "ami-0d8d212151031f51c"
    instance_type = "t2.micro"
    network_interface {
        network_interface_id = aws_network_interface.testinterface.id
        device_index         = 0
    }
}
